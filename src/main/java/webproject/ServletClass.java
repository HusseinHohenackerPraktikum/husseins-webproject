package webproject;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import de.hohenacker.praktikum.logbackmavenexemple.Message;





public class ServletClass extends HttpServlet{
	
	// Logging  
	private static final Logger logger = LoggerFactory.getLogger(ServletClass.class);
	@Inject
	private Message message;
	
		
		@Override
		protected void doGet(HttpServletRequest reqest,
				HttpServletResponse response)
		throws ServletException, IOException {
			// Zeit Angabe Methode
			response.setContentType("text/html; charset =UTF-8");
			PrintWriter out = response.getWriter();
			
			
			
			// IP Adresse von Aufrufer meinerm Webproject
			String ipClient = reqest.getRemoteAddr();
			
			/*
			kopie von App.class über Maven pom.xml ausgerufen wird, weil Dependency in webprojeckt pom.xml 
			eingetragen ist.
			 */ 
			
			message.sayHello();
			 
			
			String title = " von mir ausgesuchte Zeit:  ";
			String ipAdr = " meine IP Adresse: ";
			String Uhr = "die local Zeitangabe";
			String docType = "<!DOCTYPE HTML>\n";
			String Zeitvonmir = "      <-SimpleDateFormat von mir "	;
			out.println(docType + "<html>\n"
					+ "<head><title>" + title +   "</title></head>\n"
					+ "<body bgcolor=\"#f100fofo\">\n"
					+ "<h1 align=\"center\">" + title +  "</h1>\n"
					+ "<h2 align=\"center\">" + printSimpleDateFormat() +  "</h2>\n"
					// IP Adresse
					
					
					+ "<h4 align=\"center\">" + ipAdr + "</h4>\n"
					+ "<h4 align=\"center\">" + meineIPAdresse() + "</h4>\n"
					+ "<h4 align=\"center\">" + ipClient + "</h4>\n"
					+ "<h6 align=\"center\">" + "Zeitangabe von mir" + "</h6>\n"
					+ "<h6 align=\"center\">" + uhrangabe() + Zeitvonmir + "</h6>\n"
					+ "</body></html>");
			
			


			logger.info("doGet wurde ausgerufen" );
			
			logger.info("meine IP - Adresse lautet:    " + meineIPAdresse());
		
			logger.info("IP- Adresse von Client lautet:   " + ipClient );
			logger.info(" Zeit des Zugriffes ist :   " + uhrangabe());
	
		}
		
		
		// Uhr von mir
		protected String uhrangabe() {
			
			
			LocalTime uhr = LocalTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
			 String zeit = uhr.format(formatter);
								
			return zeit;
		}
		
		
		
		protected String meineIPAdresse () {
			AdresseIP myIP = new AdresseIP();
			String ip = myIP.getOwnerIP();
			return ip;
		}
		
		
		protected String  printSimpleDateFormat()  {		
		       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd   HH:mm:ss");
		       Date currentTime = new Date();
		     String  resultvalue = formatter.format(currentTime);
		       return resultvalue ;
		   }
		   
		 @Override
	     public void init(ServletConfig config) throws ServletException {
	         super.init(config);
	SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, 
	config.getServletContext());
	     }
		
		@Override
		public void destroy() {
			System.out.println("Servlet " + this.getServletName()
			+ " has stopped ");
		}	
	
}
