package webproject;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(
		urlPatterns ="/annotat"
		)

public class AnnotationServlet extends HttpServlet {

	
	// Logging  
		private static final Logger logger = LoggerFactory.getLogger(AnnotationServlet.class);
		
		@Override
		protected void doGet(HttpServletRequest reqest,
				HttpServletResponse response)
		throws ServletException, IOException {
			
			response.setContentType("text/html; charset =UTF-8");
			PrintWriter out = response.getWriter();
			
			
			
			
			
			
			String title = " Annotation Versuche :  ";
			String webSeite = " irgend-eine Webseite:  ";
			String docType = "<!DOCTYPE HTML>\n";
			out.println(docType + "<html>\n"
					+ "<head><title>" + title +   "</title></head>\n"
					+ "<body bgcolor=\"#f100rtffo\">\n"
					+ "<h1 align=\"center\">" + title +  "</h1>\n"
					+ "<h2 align=\"center\">" + out	+ "</h2>\n"
					// IP Adresse
					
					+ "<h4 align=\"center\">" + webSeite + "</h4>\n"
					//+ "<h4 align=\"center\">" + meineIPAdresse() + "</h4>\n"
					//+ "<h4 align=\"center\">" + ipClient + "</h4>\n"
					+ "</body></html>");
			
			


			logger.info("AnnotationServlet wurde ausgerufen" );
			
			
		}
		
		@Override
		public void init() throws ServletException {
			logger.warn("Servlet " + this.getServletName()
					+ " has started");
		}
		
		
		
		
		
}
