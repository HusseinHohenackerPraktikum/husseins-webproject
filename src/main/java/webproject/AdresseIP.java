package webproject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import org.omg.PortableInterceptor.RequestInfo;

public class AdresseIP {

	InetAddress ia;
	RequestInfo ipC;
	double krFlaeche;
	/*
	AdresseIP (RequestInfo ipClint){
		this.ipC = ipClint;
	}
	*/

	AdresseIP() {
		try {
			ia = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	
	
	public String getOwnerHostName() {

		return ia.getHostName();

	}

	public String getOwnerIP() {
		String ip = "";

		byte[] bytes = ia.getAddress();
		int nraByte = 1;
		for (byte aByte : bytes) {

			if (nraByte > 1) {

				ip += ".";
			}

			if (aByte < 0) {

				ip += (aByte + 256);
			} else {
				ip = ip + aByte;
			}
			nraByte++;

		}
		return ip;

	}

	public String getOwnerNetworkDeviceName() {
		
			NetworkInterface ni;
			try {
				ni = NetworkInterface.getByInetAddress(ia);
				if (ni != null) {
					return ni.getDisplayName();
				}
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		return null;
	}
	

	public static void main(String[] args) {
		AdresseIP aIP = new AdresseIP();
		System.out.println("Host-Name:   " + aIP.getOwnerHostName());
		System.out.println("Device-Name:  " + aIP.getOwnerNetworkDeviceName());
		System.out.println("IP-Adresse:   " + aIP.getOwnerIP());
		
		String eingabe = JOptionPane.showInputDialog( "Bitte um Eingabe von Radius");
		//eingabe = eingabe.replace(",", ".");
		double radius = Double.parseDouble(eingabe);
		double Flaeche = radius*radius*3.14;	

		System.out.println("Fläche: " + Flaeche );
	}

}
