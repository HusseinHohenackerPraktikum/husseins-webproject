package demo.rest.model;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

@javax.persistence.SequenceGenerator(name = "zeitschrift_seq", sequenceName = "zeitschrift_seq", allocationSize = 30)
@Entity
@Table(name = "zeitschriften")

public class Zeitschrift {

	@Id
	@GeneratedValue(generator = "zeitschrift_seq")
	@Column(name = "id")
	@Access(AccessType.PROPERTY)
	@Getter
	@Setter
	private Integer id;

	@Getter
	@Setter
	@Column(name = "name", length = 20)
	private String name;

	@Getter
	@Setter
	@Column(name = "standort", length = 20)
	private String standort;

	@Getter
	@Setter
	@Column(name = "anzahl_artikeln")
	private int anzahlArtikeln;

	@Getter
	@Setter
	@Column(name = "preis_eur")
	private double preis;

	@Getter
	@Setter
	@Column(name = "eur_umsatz_monat")
	private int umsatzMonat;

	@Version
	@Getter
	@Setter
	@Column(name = "version")
	private Integer version;

	/*
	 * 
	 * 1-Seite:
	 * 
	 * @Getter
	 * 
	 * @OneToMany(mappedBy = "auftrag", fetch = FetchType.LAZY) private
	 * Set<Material> materialSet = new HashSet<Material>();
	 * 
	 * public void addMaterial(Material material) { material.setAuftrag(this);
	 * materialSet.add(material); }
	 */
	@Getter
	@OneToMany(mappedBy = "userZeitschrift", fetch = FetchType.LAZY)
	// mappedBy = "userZeitschrift" Kehrseite von User ManyToOne
	private List<User> userList;

	/*
	 * obere Code ersetzt folgende Methoden public List getUserList() { return
	 * userList; } private void setUserList(List userList) { this.userList =
	 * userList; }
	 * 
	 */

	/*
	 * 
	 * int id; String name; String standort; int anzahl_artikeln; double preis; int
	 * umsatzMonat;
	 * 
	 */

	@Override
	public String toString() {
		return "ZeitschriftEntity [id=" + id + ", name=" + name + ", standort=" + standort + ", anzahlArtikeln="
				+ anzahlArtikeln + ", preis=" + preis + ", umsatzMonat=" + umsatzMonat + "]";
	}

}
