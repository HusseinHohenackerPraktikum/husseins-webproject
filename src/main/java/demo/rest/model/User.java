package demo.rest.model;

import java.time.LocalDate;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

@javax.persistence.SequenceGenerator(name = "usertable_seq", sequenceName = "usertable_seq", allocationSize = 20)
@Entity
@Table(name = "usertable"
// , indexes= {@index(name="indx_usertable_city", columnList="city"),
// @index(name="indx_usertable_state", columnList="state")}
)

public class User {
	@Id
	@GeneratedValue(generator = "usertable_seq")
	@Column(name = "id")
	@Access(AccessType.PROPERTY)
	@Getter
	@Setter // von Sequence wird eingesetzt
	private Integer id;

	@Getter
	@Setter
	@Column(name = "vorname", length = 20)
	private String vorname;

	@Getter
	@Setter
	@Column(name = "email", length = 30)
	private String email;

	@Getter
	@Setter
	@Column(name = "gebdatum")
	private LocalDate gebdatum;

	@Getter
	@Setter
	@Column(name = "city", length = 20)
	private String city;

	@Getter
	@Setter
	@Column(name = "state", length = 5)
	private String state;

	@Version
	@Getter
	@Setter
	@Column(name = "version")
	private Integer version;

	/*
	 * Zeitschrift Name ? Darf ich hier aufrufen?
	 * 
	 * @Getter
	 * 
	 * @Setter
	 * 
	 * @Column(name = "name", length = 20) private String name;
	 */
	/*
	 * N-Seite:
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "auftrag_id")
	 * 
	 * @Getter
	 * 
	 * @Setter private Auftrag auftrag;
	 * 
	 * 
	 * 
	 */
	// FetchType.EAGER es wird Zeitschrift geladen
	// FetchType.LAZY es wird keine Zeitschrift geladen

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "zeitschriften_id")
	@Getter
	@Setter
	private Zeitschrift userZeitschrift;

	@Override
	public String toString() {
		return "User [id=" + id + ", vorname=" + vorname + ", email=" + email + ", gebdatum=" + gebdatum + ", city="
				+ city + ", state=" + state + ", userZeitschrift=" + userZeitschrift + "]";
	}

	/*
	 * davor war folgendes
	 * 
	 * @Override public String toString() { return "User [id=" + id + ", name=" +
	 * vorname + ", email=" + email + ", gebdatum=" + gebdatum + ", city=" + city +
	 * ", state=" + state + "]"; }
	 * 
	 */
	/*
	 * obere Code ersetzt folgende Methoden public Zeitschrift getUserZeitschrift()
	 * { return userZeitschrift; } public void setUserZeitschrift(Zeitschrift
	 * userZeitschrift) { this.userZeitschrift = userZeitschrift; }
	 * 
	 */

	// public Integer getId() {
	// return id;
	// }
	//
	// public void setId(Integer id) {
	// this.id = id;
	// }
	//
	// public String getName() {
	// return name;
	// }
	//
	// public void setName(String name) {
	// this.name = name;
	// }
	//
	// public String getEmail() {
	// return email;
	// }
	//
	// public void setEmail(String email) {
	// this.email = email;
	// }
	//
	// public Date getBirthDate() {
	// return gebdatum;
	// }
	//
	// public void setBirthDate(Date gebdatum) {
	// this.gebdatum = gebdatum;
	// }
	//
	// public String getCity() {
	// return city;
	// }
	//
	// public void setCity(String city) {
	// this.city = city;
	// }
	//
	// public String getState() {
	// return state;
	// }
	//
	// public void setState(String state) {
	// this.state = state;
	// }

	/*
	 * User Beispiele UserRequest über Rest-Client
	 * 
	 * { "user" : { "name" : "RECK" , "email" : "reck@gadf.de" , "gebdatum" :
	 * "4566-06-24" , "city" : "Manda" , "state" : "GHJ" }}
	 * 
	 * { "user" : { "name" : "Richard" , "email" : "richard@nWR.org" , "gebdatum" :
	 * "1354-04-01" , "city" : "New Bance" , "state" : "DAG" }}
	 * 
	 * { "user" : { "name" : "Tone" , "email" : "tone@gadsd.com" , "gebdatum" :
	 * "2345-03-05" , "city" : "ALGA" , "state" : "WERT" }}
	 * 
	 * { "user" : { "name" : "Miki" , "email" : "miki@t-online.de" , "gebdatum" :
	 * "2455-12-07" , "city" : "Sena" , "state" : "Noki" }}
	 * 
	 * { "user" : { "name" : "Rock" , "email" : "rock@gfn.com" , "gebdatum" :
	 * "1954-11-29" , "city" : "Ulm" , "state" : "Bayern" }}
	 * 
	 * { "user" : { "name" : "tERck" , "email" : "Oerfd@nWR.org" , "gebdatum" :
	 * "0235-09-30" , "city" : "Botnag" , "state" : "Essen" }}
	 * 
	 * 
	 * 
	 * 
	 * User Beispiel über UserDTO über REst-Client !!! State >= 5 zeichen
	 * (varchar(5) { "vorname": "Nashel", "email": "Nashel@nWR.org", "gebdatum":
	 * "4235-09-30", "city": "Narpol", "state": "Habim" }
	 * 
	 * 
	 * 
	 * User Mit ZEITSCHRIFT EINTRAG Syntax { "vorname": "Mirko", "email":
	 * "nurlO@gmx.com", "gebdatum": "1967-09-30", "city": "Singal2", "state":
	 * "Zoom", "userZeitschrift": {"id": 22} }
	 * 
	 * 
	 * 
	 */
}
