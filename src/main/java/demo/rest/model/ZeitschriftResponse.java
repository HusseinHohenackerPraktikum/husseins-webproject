package demo.rest.model;

import java.util.List;

public class ZeitschriftResponse {

	private List zeitschriften;
	private String errorMessage;
	private Boolean success = true;

	public List getZeitschriften() {
		return zeitschriften;
	}

	public void setZeitschriften(List zeitschriften) {
		this.zeitschriften = zeitschriften;
	}

	public Boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
