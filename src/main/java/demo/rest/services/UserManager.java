package demo.rest.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import demo.rest.services.dto.UserDTO;

@Consumes("application/json")
@Produces("application/json")
@Path("/user")
public interface UserManager {

	@GET
	@Path("/{id}")
	public UserDTO fetchUserById(@PathParam("id") Integer id);

	@GET
	@Path("/")
	public List<UserDTO> fetchAllUsers();

	@POST
	@Path("/")
	public UserDTO insertUser(UserDTO userDto);

	@PUT
	@Path("/{id}")
	public UserDTO updateUser(@PathParam("id") Integer id, UserDTO userDto);

	@DELETE
	@Path("/{id}")
	public void deleteUser(@PathParam("id") Integer id);

}
