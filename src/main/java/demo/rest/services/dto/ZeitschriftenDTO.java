package demo.rest.services.dto;

import lombok.Getter;
import lombok.Setter;

public class ZeitschriftenDTO {

	@Getter
	@Setter
	private Integer id;

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private String standort;

	@Getter
	@Setter
	private int anzahlArtikeln;

	@Getter
	@Setter
	private double preis;

	@Getter
	@Setter
	private int umsatzMonat;

	@Getter
	@Setter
	private Integer version;

}
