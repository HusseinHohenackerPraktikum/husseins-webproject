package demo.rest.services.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO {

	@Getter
	@Setter
	private Integer id;

	@Getter
	@Setter
	private String vorname;

	@Getter
	@Setter
	private LocalDate gebdatum;

	@Getter
	@Setter
	private String city;

	@Getter
	@Setter
	private String state;

	@Getter
	@Setter
	private String email;

	@Getter
	@Setter
	private ZeitschriftenDTO userZeitschrift;

	@Getter
	@Setter
	private Integer version;

}
