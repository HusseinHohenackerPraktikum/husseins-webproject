package demo.rest.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import demo.rest.dao.ZeitschriftManagerDao;
import demo.rest.model.Zeitschrift;
import demo.rest.services.ZeitschriftManager;
import demo.rest.services.dto.ZeitschriftenDTO;

public class ZeitschriftManagerService implements ZeitschriftManager {

	// Logging
	private static final Logger logger = LoggerFactory.getLogger(ZeitschriftManagerService.class);

	// von Interface stammt, um alle Methoden aus Interface immer wieder abrufen
	// können
	private ZeitschriftManagerDao zeitschriftDao;

	public ZeitschriftManagerDao getZeitschriftDao() {
		return zeitschriftDao;
	}

	public void setZeitschriftDao(ZeitschriftManagerDao zeitschriftDao) {
		this.zeitschriftDao = zeitschriftDao;
	}

	public ZeitschriftenDTO fetchZeitschriftById(Integer id) {
		ZeitschriftenDTO dto = new ZeitschriftenDTO();
		try {
			Zeitschrift entity = getZeitschriftDao().fetchZeitschriftByID(id);
			dto = konvertEntityToDTO(entity);
		} catch (Exception e) {
			logger.error("fetchZeitschriftById in ZeitschriftManagerService.java " + id, e);
		}
		return dto;

	}

	public List<ZeitschriftenDTO> fetchAllZeitschriften() {

		logger.info(" ZeitschriftManagerService Methode fetchAllZeitschriften");

		List<ZeitschriftenDTO> allPapares = new ArrayList<>();

		try {
			List<Zeitschrift> entityZeitschrift = zeitschriftDao.fetchAllZeitschriften();

			logger.info(" Anzahl der Einträgen " + entityZeitschrift.size());
			for (Zeitschrift eineZeitschrift : entityZeitschrift) {
				ZeitschriftenDTO dto = konvertEntityToDTO(eineZeitschrift);
				allPapares.add(dto);

			}

		} catch (Exception e) {
			logger.error("   Fehler in fetchAllZeitschriften   " + e);
		}

		return allPapares;
	}

	public ZeitschriftenDTO insertZeitschrift(ZeitschriftenDTO zeitschriftDTO) {

		ZeitschriftenDTO dto = new ZeitschriftenDTO();

		logger.info("insertZeitschrift in ...ManagerService");
		try {
			Zeitschrift entity = konvertDTOtoEntity(zeitschriftDTO);
			entity = getZeitschriftDao().insertZeitschrift(entity);
			dto = konvertEntityToDTO(entity);

			logger.info("...konvertEntityToDTO in insertZeitschrift");
		} catch (Exception e) {
			logger.error("  Fehler in insertZeitschrift in ZeitschriftManagerService.java ", e);
			// TODO: handle exception
		}

		return dto;
	}

	/*
	 * public UserDTO updateUser(Integer id, UserDTO userDto) {
	 * logger.info("updateUser  " + id);
	 * 
	 * try { userDto.setId(id);
	 * 
	 * User toUpdateUser = konvertDTOtoEntity(userDto);
	 * 
	 * toUpdateUser = getUserDao().updateUser(toUpdateUser);
	 * 
	 * /* boolean "true" wird für die konvertEntityToDTO verwendet, um mit oder ohne
	 * Zeitschrift die Methode auszuführen.
	 */
	/*
	 * UserDTO userUPdto = konvertEntityToDTO(getUserDao().fetchUserById(id), true);
	 * // getUserDao().fetchUserById(id) => liefert mir User mit zugewiesener //
	 * Zeitschrift
	 * 
	 * return userUPdto; } catch (Exception e) {
	 * logger.error("catch-Block insertUser Fehler in updateUser", e);
	 * 
	 * } return null;
	 * 
	 * }
	 */
	public ZeitschriftenDTO updateZeitschrift(Integer id, ZeitschriftenDTO zeitschriftDTO) {
		logger.info("  updateZeitschrift  " + id);

		try {
			zeitschriftDTO.setId(id);
			Zeitschrift toUpdateZeitschrift = konvertDTOtoEntity(zeitschriftDTO);
			toUpdateZeitschrift = getZeitschriftDao().updateZeitschrift(toUpdateZeitschrift);
			ZeitschriftenDTO zeitschriftUPdto = konvertEntityToDTO(getZeitschriftDao().fetchZeitschriftByID(id));

			return zeitschriftUPdto;

		} catch (Exception e) {
			logger.error("  Catch-Block in updateZeitschrift in ...ZeitschriftManagerService.java   ", e);
		}
		return null;
	}

	public void deleteZeitschrift(Integer id) {

		try {
			getZeitschriftDao().deleteZeitschrift(id);
		} catch (Exception e) {
			logger.error("   Fehler bei deleteZeitschrift   ", e);
		}
	}

	private ZeitschriftenDTO konvertEntityToDTO(Zeitschrift entity) {
		ZeitschriftenDTO dto = new ZeitschriftenDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setAnzahlArtikeln(entity.getAnzahlArtikeln());
		dto.setPreis(entity.getPreis());
		dto.setStandort(entity.getStandort());
		dto.setUmsatzMonat(entity.getUmsatzMonat());
		dto.setVersion(entity.getVersion());

		return dto;
	}

	private Zeitschrift konvertDTOtoEntity(ZeitschriftenDTO dto) {
		Zeitschrift entity = new Zeitschrift();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setAnzahlArtikeln(dto.getAnzahlArtikeln());
		entity.setPreis(dto.getPreis());
		entity.setStandort(dto.getStandort());
		entity.setUmsatzMonat(dto.getUmsatzMonat());
		entity.setVersion(dto.getVersion());
		return entity;

	}

}
