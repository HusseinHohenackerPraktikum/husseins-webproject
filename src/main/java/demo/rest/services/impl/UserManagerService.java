package demo.rest.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import demo.rest.dao.UserManagerDao;
import demo.rest.model.User;
import demo.rest.model.Zeitschrift;
import demo.rest.services.UserManager;
import demo.rest.services.dto.UserDTO;
import demo.rest.services.dto.ZeitschriftenDTO;

public class UserManagerService implements UserManager {

	// Logging
	private static final Logger logger = LoggerFactory.getLogger(UserManagerService.class);

	private UserManagerDao userDao;

	public UserManagerDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserManagerDao userDao) {
		this.userDao = userDao;
	}

	public UserDTO fetchUserById(Integer id) {
		logger.info("fetchUserById:  " + id);
		UserDTO dto = new UserDTO();

		try {
			User entity = getUserDao().fetchUserById(id);
			/*
			 * boolean "true" wird für die konvertEntityToDTO verwendet, um mit oder ohne
			 * Zeitschrift die Methode auszuführen.
			 */
			/*
			 * 
			 * boolean mitZeitschrift = true;
			 * 
			 * if (entity.getUserZeitschrift() == null) { mitZeitschrift = false; }
			 * 
			 * if (mitZeitschrift) {
			 * 
			 * dto = konvertEntityToDTO(entity, true); } else { dto =
			 * konvertEntityToDTO(entity, false);
			 * 
			 * }
			 */
			dto = konvertEntityToDTO(entity, true);
		} catch (Exception e) {
			logger.error("Fehler in fetchUserById:   " + id, e);
		}

		return dto;
	}

	public List<UserDTO> fetchAllUsers() {
		logger.info("fetchAllUsers ");
		List<UserDTO> allUsers = new ArrayList<>();

		try {
			List<User> entityList = userDao.fetchAllUsers();

			for (User einUser : entityList) {
				/*
				 * boolean "false" wird für die konvertEntityToDTO verwendet, um mit oder ohne
				 * Zeitschrift die Methode auszuführen.
				 */
				UserDTO dto = konvertEntityToDTO(einUser, false);
				allUsers.add(dto);
			}
		} catch (Exception e) {
			logger.error("Fehler in fetchAllUsers", e);
		}
		return allUsers;
	}

	public UserDTO insertUser(UserDTO eingesetzterUserdto) {
		logger.info("insertUser");

		logger.debug("eingesetzterUserdto" + eingesetzterUserdto);
		UserDTO dto = new UserDTO();
		try {

			User entity = konvertDTOtoEntity(eingesetzterUserdto);
			logger.info("konvertDTOtoEntity" + entity);
			// User entitiy = setUserDao(insertUser());
			entity = getUserDao().insertUser(entity);
			logger.info("entity = getUserDao().insertUser(entity)" + entity);
			/*
			 * boolean "true" wird für die konvertEntityToDTO verwendet, um mit oder ohne
			 * Zeitschrift die Methode auszuführen.
			 */
			dto = konvertEntityToDTO(entity, true);
			logger.info("dto = konvertEntityToDTO(entity" + dto);

			// getUserDao().insertUser(request.getUser());
		} catch (Exception e) {

			logger.error("Fehler in insertUser", e);

		}
		return dto;

	}

	public UserDTO updateUser(Integer id, UserDTO userDto) {
		logger.info("updateUser  " + id);

		try {
			userDto.setId(id);

			User toUpdateUser = konvertDTOtoEntity(userDto);

			toUpdateUser = getUserDao().updateUser(toUpdateUser);

			/*
			 * boolean "true" wird für die konvertEntityToDTO verwendet, um mit oder ohne
			 * Zeitschrift die Methode auszuführen.
			 */
			UserDTO userUPdto = konvertEntityToDTO(getUserDao().fetchUserById(id), true);
			// getUserDao().fetchUserById(id) => liefert mir User mit zugewiesener
			// Zeitschrift

			return userUPdto;
		} catch (Exception e) {
			logger.error("catch-Block insertUser Fehler in updateUser", e);

		}
		return null;

	}

	public void deleteUser(Integer id) {
		logger.info("deleteUser" + id + " ");
		// UserResponse response = new UserResponse();

		try {

			// Delete umbauen mit id...

			getUserDao().deleteUser(id);

		} catch (Exception e) {
			logger.error("Fehler in deleteUser", e);
			// response.setSuccess(false);
			// response.setErrorMessage(e.getClass() + ": " + e.getMessage());
		}

		// return response;
	}

	/*
	 * Parameter boolean mitZeitschrift wird in der Methode definiert, damit der
	 * Client die Entscheidung trifft mit ohne id das heißt für uns, ob die
	 * if-Klausel abgearbeitet soll
	 */
	private UserDTO konvertEntityToDTO(User entity, boolean mitZeitschrift) {
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setVorname(entity.getVorname());
		dto.setEmail(entity.getEmail());
		dto.setGebdatum(entity.getGebdatum());
		dto.setCity(entity.getCity());
		dto.setState(entity.getState());

		// Zeitschriften anbinden
		/*
		 * Hole Zeitschrift Entity, konvertiere Zeitschrift to ZeitschriftDTO, setzt
		 * neue Variable konvertZeitschrift zu ZeitschriftDTO type, dann zum
		 * UserDTO(dto) setzt die erforderliche ZeitschriftDTO(variable)
		 */
		// boolean mitZeitschrift = true;

		if (mitZeitschrift) {

			Zeitschrift zeitschrift = entity.getUserZeitschrift();

			if (zeitschrift != null) {
				ZeitschriftenDTO konvertZeitschrift = konvertEntityToDto(zeitschrift);

				dto.setUserZeitschrift(konvertZeitschrift);
			}
		}
		// Version
		dto.setVersion(entity.getVersion());

		return dto;
	}

	/*
	 * boolean mit Zeitschrift=Parameter wird UserBYID ausgerufen, dann wird die
	 * code abgearbeitet
	 */
	private User konvertDTOtoEntity(UserDTO dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setVorname(dto.getVorname());
		entity.setEmail(dto.getEmail());
		entity.setGebdatum(dto.getGebdatum());
		entity.setCity(dto.getCity());
		entity.setState(dto.getState());

		if (dto.getUserZeitschrift() != null) {

			entity.setUserZeitschrift(konvertDTOtoEntity(dto.getUserZeitschrift()));
		}

		// entity.setUserZeitschrift(dto.get); wollte Zeitschrift_id mit eingeben bei
		// Insert User

		/*
		 * if (mitZeitschrift) { ZeitschriftenDTO dtoZeitschrift =
		 * dto.getUserZeitschrift(); Zeitschrift konvertToEntity =
		 * konvertDTOtoEntity(dtoZeitschrift);
		 * entity.setUserZeitschrift(konvertToEntity); }
		 */
		// Version
		entity.setVersion(dto.getVersion());
		return entity;
	}

	private ZeitschriftenDTO konvertEntityToDto(Zeitschrift entity) {
		ZeitschriftenDTO dtoZeitschriften = new ZeitschriftenDTO();

		dtoZeitschriften.setId(entity.getId());

		dtoZeitschriften.setName(entity.getName());
		dtoZeitschriften.setStandort(entity.getStandort());
		dtoZeitschriften.setAnzahlArtikeln(entity.getAnzahlArtikeln());
		dtoZeitschriften.setPreis(entity.getPreis());
		dtoZeitschriften.setUmsatzMonat(entity.getUmsatzMonat());

		return dtoZeitschriften;

	}

	private Zeitschrift konvertDTOtoEntity(ZeitschriftenDTO dto) {
		Zeitschrift entity = new Zeitschrift();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setStandort(dto.getStandort());
		entity.setAnzahlArtikeln(dto.getAnzahlArtikeln());
		entity.setPreis(dto.getPreis());
		entity.setUmsatzMonat(dto.getUmsatzMonat());
		return entity;
	}

}
