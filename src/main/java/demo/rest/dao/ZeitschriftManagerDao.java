package demo.rest.dao;

import java.util.List;

import demo.rest.model.Zeitschrift;

public interface ZeitschriftManagerDao {

	public void deleteZeitschrift(Integer id);

	public List<Zeitschrift> fetchAllZeitschriften();

	public Zeitschrift fetchZeitschriftByID(Integer id);

	public Zeitschrift insertZeitschrift(Zeitschrift insertZeitschrift);

	public Zeitschrift updateZeitschrift(Zeitschrift updateZeitschrift);

}
