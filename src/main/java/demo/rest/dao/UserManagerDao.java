package demo.rest.dao;

import java.util.List;

import demo.rest.model.User;

public interface UserManagerDao {

	public User fetchUserById(Integer id);

	public List<User> fetchAllUsers();

	public User insertUser(User user);

	public User updateUser(User user);

	public void deleteUser(Integer id);

}
