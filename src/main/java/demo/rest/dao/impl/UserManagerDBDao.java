package demo.rest.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import demo.rest.dao.UserManagerDao;
import demo.rest.model.User;

public class UserManagerDBDao implements UserManagerDao {

	// Logging
	private static Logger logger = LoggerFactory.getLogger(UserManagerDBDao.class);

	// Zugriffe auf Datenbank "usertable"
	private static final String getAllUsers = "select u from User u ";

	// select * from usertable left join zeitschriften on usertable.zeitschriften_id
	// = zeitschriften.id
	// where usertable.id = 249;
	private static final String getUserById = "select c from User c left join fetch c.userZeitschrift"
			+ " where c.id = ?1 ";
	// MAPPING IN USER.CLASS => REFERENCE zwischen TAbellen "on
	// usertable.zeitschriften_id = zeitschrift.id " =
	// REFERENCE zwischen Entity (User<-> Zeitschrift) c.userZeitschrift

	private static final String setUser = "select s from User s where s.vorname = ?1 and s.email = ?2 and "
			+ "	s.gebdatum = ?3 and s.city =?4 and s.state =?5";
	private static final String updateUser = " ";
	private static final String loeschenUser = "select d from User d where d.id=?1";

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional

	public User fetchUserById(Integer id) {
		try {
			return (User) entityManager.createQuery(getUserById).setParameter(1, id).getSingleResult();
		} catch (NonUniqueResultException e) {
			return null;
		}

	}

	@Transactional
	public List<User> fetchAllUsers() {
		List<User> usersListe = new ArrayList<>();

		return usersListe = entityManager.createQuery(getAllUsers).getResultList();
	}

	/*
	 * .setParameter(5, user.getState()).getResultList(); liefert mir eine Liste,
	 * die soll leer sein, um neue Datensatz einzufügen
	 */
	@Transactional
	public User insertUser(User user) {
		List<User> foundUsers = entityManager.createQuery(setUser).setParameter(1, user.getVorname())
				.setParameter(2, user.getEmail()).setParameter(3, user.getGebdatum()).setParameter(4, user.getCity())
				.setParameter(5, user.getState()).getResultList();

		if (foundUsers.isEmpty()) {
			return entityManager.merge(user);
		} else {
			return foundUsers.get(0);
		}
	}

	@Transactional
	public User updateUser(User user) {
		user = entityManager.merge(user);
		// .merge()-Methode entweder setzt oder updated die Usertabelle
		return user;
	}

	@Transactional
	public void deleteUser(Integer id) {
		try {
			User delUser = (User) entityManager.createQuery(loeschenUser).setParameter(1, id).getSingleResult();

			if (delUser != null) {
				entityManager.remove(delUser);
			}
		} catch (NoResultException e) {
			return;
		}

	}

}
