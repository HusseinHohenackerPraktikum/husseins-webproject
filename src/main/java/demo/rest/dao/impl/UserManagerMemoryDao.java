package demo.rest.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import demo.rest.dao.UserManagerDao;
import demo.rest.model.User;

public class UserManagerMemoryDao implements UserManagerDao {

	// Logging
	private static final Logger logger = LoggerFactory.getLogger(UserManagerMemoryDao.class);

	private int nextUserId = 0;

	List<User> users = new ArrayList<User>();

	public User fetchUserById(Integer id) {
		logger.info("fetchUserById in UserManagerMemoryDao Anzahl Users: " + users.size());

		// ich glaube, es gibt keine "users", deshalb die Schleife mit
		// NullpointerException meckert...

		for (User user : users) {
			logger.info("aktueller User: " + id + " " + user.getId() + " ausgerufener User zum Vergleich ");
			if (user.getId().equals(id)) {
				logger.info("User gefunden: " + id + " ");
				return user;
			}
		}

		throw new RuntimeException("User Not Found: " + id);
	}

	public List<User> fetchAllUsers() {

		logger.info("UserManagerMemoryDao  fetchAllUsers");
		return users;
	}

	public User insertUser(User user) {
		logger.info("UserManagerMemoryDao  insertUser");
		user.setId(nextUserId++);
		users.add(user);
		return null;
	}

	// soll/wird überschrieben
	// @Override
	public User updateUser(User user) {

		logger.info("UserManagerMemoryDao  updateUser");
		User editUser = fetchUserById(user.getId());
		// Integer editUser = user.getId();

		editUser.setGebdatum(user.getGebdatum());
		editUser.setCity(user.getCity());
		editUser.setEmail(user.getEmail());
		editUser.setVorname(user.getVorname());
		editUser.setState(user.getState());

		return editUser;
	}

	/*
	 * @Override public void updateUser(Integer id) {
	 * logger.info("UserManagerMemoryDao  updateUser");
	 * 
	 * User userUpdate = fetchUserById(id);
	 * 
	 * 
	 * 
	 * }
	 * 
	 */

	/*
	 * wird überschrieben public void deleteUser(User user) { User delUser =
	 * fetchUserById(user.getId()); users.remove(delUser); }
	 */
	@Override
	public void deleteUser(Integer id) {

		logger.info("UserManagerMemoryDao  deleteUser");
		User userloeschen = fetchUserById(id);
		users.remove(userloeschen);

	}

}
