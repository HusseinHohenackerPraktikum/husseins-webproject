package demo.rest.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import demo.rest.dao.ZeitschriftManagerDao;
import demo.rest.model.Zeitschrift;

public class ZeitschriftManagerDBDao implements ZeitschriftManagerDao {

	// Logging
	private static Logger logger = LoggerFactory.getLogger(ZeitschriftManagerDBDao.class);

	private static final String getAllPapers = "select z from Zeitschrift z";

	private static final String getPaperById = "select p from Zeitschrift p where p.id = ?1";

	private static final String setZeitschrift = "select z from Zeitschrift z where z.name =?1 and z.standort = ?2 and "
			+ "z.anzahlArtikeln = ?3 and z.preis = ?4 and z.umsatzMonat = ?5";

	private static final String loeschenZeitschrift = "select d from Zeitschrift d where d.id=?1";

	@PersistenceContext
	private EntityManager entityManagerZeitschrift;

	/*
	 * @Transactional public void deleteUser(Integer id) { try { User delUser =
	 * (User) entityManager.createQuery(loeschenUser).setParameter(1,
	 * id).getSingleResult();
	 * 
	 * if (delUser != null) { entityManager.remove(delUser); } } catch
	 * (NoResultException e) { return; }
	 * 
	 * } @see
	 * demo.rest.dao.ZeitschriftManagerDao#deleteZeitschrift(java.lang.Integer)
	 */
	@Transactional
	public void deleteZeitschrift(Integer id) {
		try {
			Zeitschrift delZeitschrift = (Zeitschrift) entityManagerZeitschrift.createQuery(loeschenZeitschrift)
					.setParameter(1, id).getSingleResult();

			if (delZeitschrift != null) {
				entityManagerZeitschrift.remove(delZeitschrift);
			}

		} catch (NoResultException e) {
			return;
		}
	}

	@Transactional
	public List<Zeitschrift> fetchAllZeitschriften() {
		List<Zeitschrift> allZeitschriften = new ArrayList<>();
		logger.info("fetchAllZeitschriften" + allZeitschriften.size());

		return allZeitschriften = entityManagerZeitschrift.createQuery(getAllPapers).getResultList();
	}

	@Transactional
	public Zeitschrift fetchZeitschriftByID(Integer id) {
		try {
			return (Zeitschrift) entityManagerZeitschrift.createQuery(getPaperById).setParameter(1, id)
					.getSingleResult();
		} catch (NonUniqueResultException e) {
			return null;
		}
	}

	@Transactional
	public Zeitschrift insertZeitschrift(Zeitschrift insertZeitschrift) {
		List<Zeitschrift> foundZeitschrift = entityManagerZeitschrift.createQuery(setZeitschrift)
				.setParameter(1, insertZeitschrift.getName()).setParameter(2, insertZeitschrift.getStandort())
				.setParameter(3, insertZeitschrift.getAnzahlArtikeln()).setParameter(4, insertZeitschrift.getPreis())
				.setParameter(5, insertZeitschrift.getUmsatzMonat()).getResultList();
		// .getResultList() wandelt Query in die Liste

		if (foundZeitschrift.isEmpty()) {
			return entityManagerZeitschrift.merge(insertZeitschrift);
		} else {
			return foundZeitschrift.get(0);
		}
	}

	@Transactional
	public Zeitschrift updateZeitschrift(Zeitschrift updateZeitschrift) {
		Zeitschrift newZeitschrift = entityManagerZeitschrift.merge(updateZeitschrift);
		return newZeitschrift;
	}

}
